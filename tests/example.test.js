const {getAutoSolveClient, CaptchaTokenRequest, ReCaptchaV2Checkbox, OpenError} = require("../dist/index");
//const {getAutoSolveClient, CaptchaTokenRequest, ReCaptchaV2Checkbox, OpenError} = require("autosolve-client");
//import { getAutoSolveClient, CaptchaTokenRequest, ReCaptchaV2Checkbox, OpenError } from "autosolve-client";

class SessionListenerImpl {

    constructor(service) {
        this._service = service;
    }

    onError(err) {
        console.log("onError", err);
    }

    onStatusChanged(status) {
        console.log("onStatusChanged", status);
    }

    onTokenCancelResponse(response) {
        console.log("onTokenCancelResponse", response);
        response.requests.forEach(request => {
            let resp = new CaptchaSolveResponse(true, undefined);
            this._service._responseMap.get(request.taskId).call(this._service, resp);
        });
    }

    onTokenResponse(response) {
        console.log("onTokenResponse", response);
        let resp = new CaptchaSolveResponse(false, response);
        this._service._responseMap.get(response.taskId).call(this._service, resp);
    }

}

class CaptchaSolveResponse {

    constructor(cancelled, response) {
        this._cancelled = cancelled;
        this._response = response;
    }

    get cancelled() {
        return this._cancelled;
    }

    get response() {
        return this._response;
    }
}

class AutoSolveService {

    constructor() {
        this._listener = new SessionListenerImpl(this);
        this._session = undefined;
        this._responseMap = new Map();
    }

    async init(clientId) {
        await getAutoSolveClient().init(clientId);
    }

    async connect(apiKey) {
        this._session = await getAutoSolveClient().getSession(apiKey);
        this._session.setListener(this._listener);
        let err = await this._session.open();
        if (err) {
            return err;
        }
        return OpenError.None;
    }

    async close() {
        if (this._session) {
            await this._session.close();
        }
    }

    async solve(tokenRequest) {
        if (!this._session) {
            throw new Error("Session is not available.");
        }
        let promise = new Promise(resolve => {
            this._responseMap.set(tokenRequest.taskId, resolve);
        });
        this._session.send(tokenRequest);
        return promise;
    }
}

async function main() {
    let clientId = String(process.env.CLIENT_ID);
    let apiKey = String(process.env.API_KEY);
    let service = new AutoSolveService();
    await service.init(clientId);
    let error = await service.connect(apiKey);
    if (error !== OpenError.None) {
        console.log("Error:", error);
        return;
    }

    let tokenRequest = new CaptchaTokenRequest();
    tokenRequest.url = "https://recaptcha.autosolve.io/version/1";
    tokenRequest.siteKey = "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b";
    tokenRequest.taskId = "test1";
    tokenRequest.version = ReCaptchaV2Checkbox;

    let response = await service.solve(tokenRequest);

    if (response.cancelled) {
        console.log("Cancelled: ", tokenRequest.taskId);
    } else {
        console.log("Response", tokenRequest.taskId);
    }

    console.log("Received response", response);

    let tokenRequest2 = new CaptchaTokenRequest();
    tokenRequest2.url = "https://recaptcha.autosolve.io/version/1";
    tokenRequest2.siteKey = "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b";
    tokenRequest2.taskId = "test2";
    tokenRequest2.version = ReCaptchaV2Checkbox;

    response = await service.solve(tokenRequest2);

    if (response.cancelled) {
        console.log("Cancelled: ", tokenRequest.taskId);
    } else {
        console.log("Success", tokenRequest.taskId);
    }

    console.log("Received response", response);

    process.stdin.resume();
}

if (require.main === module) {
    main();
}
