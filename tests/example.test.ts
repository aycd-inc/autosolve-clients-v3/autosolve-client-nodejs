import {
    CaptchaResponse,
    CaptchaTokenCancelResponse,
    CaptchaTokenRequest,
    CaptchaTokenResponse,
    getAutoSolveClient,
    OpenError,
    ReCaptchaV2Checkbox,
    Session,
    SessionListener,
    Status
} from "../src";

class SessionListenerImpl implements SessionListener {

    private readonly _service: AutoSolveService;

    constructor(service: AutoSolveService) {
        this._service = service;
    }

    onError(err: any): void {
        console.log("onError", err);
    }

    onStatusChanged(status: Status): void {
        console.log("onStatusChanged", status);
    }

    onTokenCancelResponse(response: CaptchaTokenCancelResponse): void {
        console.log("onTokenCancelResponse", response);
        response.requests?.forEach(request => {
            let resp = new CaptchaSolveResponse(true, undefined);
            this._service._responseMap.get(request.taskId)?.call(this._service, resp);
        });
    }

    onTokenResponse(response: CaptchaTokenResponse): void {
        console.log("onTokenResponse", response);
        let resp = new CaptchaSolveResponse(false, response);
        this._service._responseMap.get(response.taskId)?.call(this._service, resp);
    }

}

class CaptchaSolveResponse {

    private readonly _cancelled: boolean;
    private readonly _response: CaptchaResponse | undefined;

    constructor(cancelled: boolean, response: CaptchaResponse | undefined) {
        this._cancelled = cancelled;
        this._response = response;
    }

    get cancelled(): boolean {
        return this._cancelled;
    }

    get response(): CaptchaResponse | undefined {
        return this._response;
    }
}

class AutoSolveService {
    private readonly _listener: SessionListener;
    private _session: Session | undefined;
    _responseMap: Map<string, Function> = new Map();

    constructor() {
        this._listener = new SessionListenerImpl(this);
    }

    public async init(clientId: string) {
        await getAutoSolveClient().init(clientId);
    }

    public async connect(apiKey: string): Promise<OpenError> {
        this._session = await getAutoSolveClient().getSession(apiKey);
        this._session.setListener(this._listener);
        let err = await this._session.open();
        if (err) {
            return err;
        }
        return OpenError.None;
    }

    public async close(): Promise<void> {
        if (this._session) {
            await this._session.close();
        }
    }

    public async solve(tokenRequest: CaptchaTokenRequest): Promise<CaptchaSolveResponse> {
        if (!this._session) {
            throw new Error("Session is not available.");
        }
        let promise = new Promise<CaptchaSolveResponse>(resolve => {
            this._responseMap.set(tokenRequest.taskId, resolve);
        });
        this._session.send(tokenRequest);
        return promise;
    }
}

async function main() {
    let clientId = String(process.env.CLIENT_ID);
    let apiKey = String(process.env.API_KEY);
    let service = new AutoSolveService();
    await service.init(clientId);
    let error = await service.connect(apiKey);
    if (error !== OpenError.None) {
        console.log("Error:", error);
        return;
    }

    let tokenRequest = new CaptchaTokenRequest();
    tokenRequest.url = "https://recaptcha.autosolve.io/version/1";
    tokenRequest.siteKey = "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b";
    tokenRequest.taskId = "test1";
    tokenRequest.version = ReCaptchaV2Checkbox;

    let response = await service.solve(tokenRequest);

    if (response.cancelled) {
        console.log("Cancelled: ", tokenRequest.taskId);
    } else {
        console.log("Response", tokenRequest.taskId);
    }

    console.log("Received response", response);

    let tokenRequest2 = new CaptchaTokenRequest();
    tokenRequest2.url = "https://recaptcha.autosolve.io/version/1";
    tokenRequest2.siteKey = "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b";
    tokenRequest2.taskId = "test2";
    tokenRequest2.version = ReCaptchaV2Checkbox;

    response = await service.solve(tokenRequest2);

    if (response.cancelled) {
        console.log("Cancelled: ", tokenRequest.taskId);
    } else {
        console.log("Success", tokenRequest.taskId);
    }

    console.log("Received response", response);

    process.stdin.resume();
}

if (require.main === module) {
    main();
}
