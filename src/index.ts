"use strict";

import {Channel, Connection, ConsumeMessage} from "amqplib";

export interface Client {

    init(clientId: string): Promise<void>

    getSession(apiKey: string): Promise<Session>

    hasSession(session: Session): boolean;

    removeSession(session: Session): void;

    setMultiSessionEnabled(multiSessionEnabled: boolean): void;

    getClientId(): string;
}

export interface SessionListener {
    onStatusChanged(status: Status): void;

    onError(err: any): void;

    onTokenResponse(response: CaptchaTokenResponse): void;

    onTokenCancelResponse(response: CaptchaTokenCancelResponse): void;
}

export interface Session {

    open(): Promise<OpenError>;

    close(): Promise<void>;

    send(request: CaptchaTokenRequest): void;

    cancelOne(taskId: string): void;

    cancelMany(taskIds: string[]): void;

    cancelAll(): void;

    setListener(listener: SessionListener): void;

    getApiKey(): string;
}

export enum OpenError {
    None = "",
    InvalidApiKeyError = "the apiKey for this session is not valid",
    InvalidSessionError = "the session is no longer valid",
    ConnectError = "failed to connect to autosolve",
    ConnectionPendingError = "the session is already attempting to open a connection",
    ServerError = "failed to reach auth server",
}

export interface CaptchaResponse {

}

export class CaptchaTokenRequest {
    taskId!: string;
    apiKey!: string;
    createdAt!: number;
    url: string | undefined;
    siteKey: string | undefined;
    version: number | undefined;
    action: string | undefined;
    minScore: number | undefined;
    proxy: string | undefined;
    proxyRequired: boolean | undefined;
    userAgent: string | undefined;
    cookies: string | undefined;
    renderParameters: Record<string, string> | undefined;
    metadata: Record<string, string> | undefined;
}

export class CaptchaTokenCancelRequest {
    taskId!: string;
    apiKey!: string;
    createdAt!: number;
    taskIds: string[] | undefined;
    responseRequired: boolean | undefined;
}

export class CaptchaTokenResponse implements CaptchaResponse {
    taskId!: string;
    apiKey!: string;
    createdAt!: number;
    request: CaptchaTokenRequest | undefined;
    token: string | undefined;
}

export class CaptchaTokenCancelResponse implements CaptchaResponse {
    requests: CaptchaTokenRequest[] | undefined;
}

export enum Status {
    Connecting = "Connecting",
    Connected = "Connected",
    Reconnecting = "Reconnecting",
    Disconnected = "Disconnected",
}

export const ReCaptchaV2Checkbox = 0;
export const ReCaptchaV2Invisible = 1;
export const ReCaptchaV3 = 2;
export const GeeTest = 5;
export const ReCaptchaV3Enterprise = 6;
export const ReCaptchaV2Enterprise = 7;
export const FunCaptcha = 8;

const amqp = require('amqplib');
const axios = require('axios');
const hostname = "amqp.autosolve.aycd.io"
const vhost = "oneclick"
const directExchangePrefix = "exchanges.direct"
const fanoutExchangePrefix = "exchanges.fanout"

const responseQueuePrefix = "queues.response.direct"

const requestTokenRoutePrefix = "routes.request.token"
const requestTokenCancelRoutePrefix = "routes.request.token.cancel"
const responseTokenRoutePrefix = "routes.response.token"
const responseTokenCancelRoutePrefix = "routes.response.token.cancel"

const autoAckQueue = true
const exclusiveQueue = false

const reconnectTimeouts = [2, 3, 5, 8, 13, 21, 34]
const connectMinDelay = 5
const maxReconnectAttempts = 100

class ClientImpl implements Client {
    private _clientId!: string;

    private _sessions: Session[] = [];
    private _multiSessionEnabled: boolean = false;

    async init(clientId: string): Promise<void> {
        if (this._clientId) {
            throw new Error("Client already initialized");
        } else if (!clientId || ((clientId = clientId.trim()).length == 0)) {
            throw new Error("ClientId is not valid");
        }
        this._clientId = clientId;
    }

    async getSession(apiKey: string): Promise<Session> {
        if (!this._clientId) {
            throw new Error("Client not initialized");
        }
        if (apiKey) {
            apiKey = apiKey.trim();
            if (apiKey.length > 0) {
                let session: Session | undefined = this._sessions.find(s => s.getApiKey() === apiKey);
                if (session) {
                    return session;
                } else {
                    session = createSession(this, apiKey);
                    if (this._multiSessionEnabled) {
                        this._sessions.push(session);
                    } else {
                        for (let s of this._sessions) {
                            await s.close();
                        }
                        this._sessions = [session];
                    }
                }
                return session;
            }
        }
        throw new Error("Invalid API key");
    }

    async removeSession(session: Session): Promise<void> {
        if (this._multiSessionEnabled) {
            let index: number = this._sessions.indexOf(session);
            if (index >= 0) {
                this._sessions.splice(index, 1);
            }
        } else {
            throw new Error("Multi-Session is not enabled");
        }
    }

    hasSession(session: Session): boolean {
        return this._sessions.indexOf(session) >= 0;
    }

    setMultiSessionEnabled(enabled: boolean): void {
        this._multiSessionEnabled = enabled;
    }

    getClientId(): string {
        return this._clientId;
    }
}

const client = new ClientImpl();

export function getAutoSolveClient(): Client {
    return client;
}

class AmqpConfig {
    username!: string;
    password!: string;
    directExchangeName!: string;
    fanoutExchangeName!: string;
    responseQueueName!: string;
    responseTokenRouteKey!: string;
    responseTokenCancelRouteKey!: string;
    requestTokenRouteKey!: string;
    requestTokenCancelRouteKey!: string;
}

class EmptySessionListenerImpl implements SessionListener {
    onError(err: any): void {
    }

    onStatusChanged(status: Status): void {
    }

    onTokenCancelResponse(response: CaptchaTokenCancelResponse): void {
    }

    onTokenResponse(response: CaptchaTokenResponse): void {
    }

}

function createSession(client: Client, apiKey: string) {
    return new SessionImpl(client, apiKey);
}

class SessionImpl implements Session {

    private readonly _apiKey: string;
    private readonly _client: Client;
    private _listener: SessionListener = new EmptySessionListenerImpl();
    private _status: Status = Status.Disconnected;
    private _requests: CaptchaTokenRequest[] = [];
    private _cancelRequests: CaptchaTokenCancelRequest[] = [];
    private _amqpConfig!: AmqpConfig;
    private _amqpConnection!: Connection;
    private _directChannel!: Channel;
    private _fanoutChannel!: Channel;
    private _lastConnectTime: number = 0;
    private _connecting: boolean = false;
    private _explicitShutdown: boolean = false;
    private _sendTokenInterval: NodeJS.Timeout | undefined = undefined;
    private _sendTokenCancelInterval: NodeJS.Timeout | undefined = undefined;

    constructor(client: Client, apiKey: string) {
        this._apiKey = apiKey;
        this._client = client;
    }

    public getApiKey(): string {
        return this._apiKey;
    }

    async setListener(listener: SessionListener): Promise<void> {
        if (listener) {
            this._listener = listener;
        } else {
            this._listener = new EmptySessionListenerImpl();
        }
    }

    async cancelAll(): Promise<void> {
        let request = new CaptchaTokenCancelRequest();
        request.apiKey = this._apiKey;
        request.createdAt = getCurrentUnixTime();
        request.responseRequired = false;
        this._cancelRequests.push(request);
    }

    async cancelMany(taskIds: string[]): Promise<void> {
        if (taskIds.length === 0) {
            return;
        }
        let request = new CaptchaTokenCancelRequest();
        request.apiKey = this._apiKey;
        request.createdAt = getCurrentUnixTime();
        request.responseRequired = true;
        request.taskIds = taskIds;
        this._cancelRequests.push(request);
    }

    async cancelOne(taskId: string): Promise<void> {
        await this.cancelMany([taskId]);
    }

    async send(request: CaptchaTokenRequest): Promise<void> {
        this._requests.push(request);
    }

    async open(): Promise<OpenError> {
        if (this._connecting) {
            return OpenError.ConnectionPendingError;
        }
        this._connecting = true;
        this._explicitShutdown = false;
        await this.setStatus(Status.Connecting);
        let error = await this.doOpen();
        if (error) {
            this._connecting = false;
            await this.setStatus(Status.Disconnected);
            return error;
        }
        await this.setStatus(Status.Connected);
        this._connecting = false;
        return OpenError.None;
    }

    async close(): Promise<void> {
        this._explicitShutdown = true;
        return await this.doClose();
    }

    private async doClose(): Promise<void> {
        if (this._amqpConnection) {
            try {
                await this._amqpConnection.close();
            } catch (alreadyClosed) {
            }
        }
        if (this._sendTokenInterval) {
            clearInterval(this._sendTokenInterval);
        }
        if (this._sendTokenCancelInterval) {
            clearInterval(this._sendTokenCancelInterval);
        }
    }

    private async doOpen(): Promise<OpenError> {
        try {
            await this.doClose();
            if (!this.isValidSession()) {
                return OpenError.InvalidSessionError;
            }
            let currentTime = getCurrentUnixTime();
            let elapsedTime = connectMinDelay - (currentTime - this._lastConnectTime);
            if (elapsedTime > 0) {
                await new Promise(r => setTimeout(r, elapsedTime));
            }
            this._lastConnectTime = getCurrentUnixTime();
            let openError = await this.authenticate();
            if (openError !== OpenError.None) {
                return openError;
            }
            return await this.connect();
        } catch (err) {
            await this.sendError(err);
            return OpenError.ConnectError;
        }
    }

    private async authenticate(): Promise<OpenError> {
        try {
            let response = await axios.get('https://autosolve-dashboard-api.aycd.io/api/v1/auth/verify?clientId=' + this._client.getClientId() + '&apiKey=' + this._apiKey);
            if (response && response.status === 200) {
                let data = response.data;
                if (data) {
                    let username = data.username;
                    let password = data.password;
                    if (username && password) {
                        let fmtPassword = replaceAllDashes(password);
                        let fmtApiKey = replaceAllDashes(this._apiKey);
                        this._amqpConfig = new AmqpConfig();
                        this._amqpConfig.username = username;
                        this._amqpConfig.password = password;
                        this._amqpConfig.directExchangeName = createKeyWithAccountId(
                            directExchangePrefix, username);
                        this._amqpConfig.fanoutExchangeName = createKeyWithAccountId(
                            fanoutExchangePrefix, username);
                        this._amqpConfig.responseQueueName = createKeyWithAccountIdAndApiKey(
                            responseQueuePrefix, username, fmtApiKey);
                        this._amqpConfig.responseTokenRouteKey = createKeyWithAccountIdAndApiKey(
                            responseTokenRoutePrefix, username, fmtApiKey);
                        this._amqpConfig.responseTokenCancelRouteKey = createKeyWithAccountIdAndApiKey(
                            responseTokenCancelRoutePrefix, username, fmtApiKey);
                        this._amqpConfig.requestTokenRouteKey = createKeyWithAccessToken(
                            requestTokenRoutePrefix, fmtPassword);
                        this._amqpConfig.requestTokenCancelRouteKey = createKeyWithAccessToken(
                            requestTokenCancelRoutePrefix, fmtPassword);
                        return OpenError.None;
                    }
                }
            } else {
                return OpenError.InvalidApiKeyError;
            }
        } catch (err) {
            await this.sendError(err);
            // @ts-ignore
            let response = err.response;
            if (response) {
                let status = response.status;
                if (status === 400 || status == 401) {
                    return OpenError.InvalidApiKeyError;
                }
            }
        }
        return OpenError.ServerError;
    }

    private async setStatus(status: Status) {
        if (this._status !== status) {
            this._status = status;
            this._listener.onStatusChanged(status);
        }
    }

    private async sendError(err: any) {
        this._listener.onError(err);
    }

    private async onMessage(message: ConsumeMessage) {
        try {
            let data = message.content.toString();
            let response = JSON.parse(data);
            let routingKey = message.fields.routingKey
            if (routingKey === this._amqpConfig.responseTokenRouteKey) {
                await this.onTokenResponse(response as CaptchaTokenResponse);
            } else if (routingKey === this._amqpConfig.responseTokenCancelRouteKey) {
                await this.onCancelResponse(response as CaptchaTokenCancelResponse);
            }
        } catch (err) {
            await this.sendError(err);
        }
    }

    private async onTokenResponse(response: CaptchaTokenResponse) {
        this._listener.onTokenResponse(response);
    }

    private async onCancelResponse(response: CaptchaTokenCancelResponse) {
        this._listener.onTokenCancelResponse(response);
    }

    private async connect(): Promise<OpenError> {
        try {
            let conn = await amqp.connect("amqp://" + this._amqpConfig.username + ":" + this._amqpConfig.password + "@" + hostname + ":5672/" + vhost + "?heartbeat=10");
            conn.on('error', (err: any) => {
                this.reconnect(err, 0);
            });
            let directChannel = await conn.createChannel();
            directChannel.on('error', (err: any) => {
                this.reconnect(err, 0);
            });
            let fanoutChannel = await conn.createChannel();
            fanoutChannel.on('error', (err: any) => {
                this.reconnect(err, 0);
            });
            this._amqpConnection = conn;
            this._directChannel = directChannel;
            this._fanoutChannel = fanoutChannel;
            await directChannel.bindQueue(this._amqpConfig.responseQueueName, this._amqpConfig.directExchangeName, this._amqpConfig.responseTokenRouteKey);
            await directChannel.bindQueue(this._amqpConfig.responseQueueName, this._amqpConfig.directExchangeName, this._amqpConfig.responseTokenCancelRouteKey);
            await directChannel.consume(this._amqpConfig.responseQueueName, this.onMessage.bind(this),
                {noAck: autoAckQueue, exclusive: exclusiveQueue, noWait: false, noLocal: false});
            await this.setupSendIntervals()
        } catch (err) {
            await this.sendError(err);
            await this.doClose();
            return OpenError.ConnectError;
        }
        return OpenError.None;
    }

    private async reconnect(err: any, attempt: number) {
        if (!this._explicitShutdown && (!this._connecting || attempt > 0)) {
            this._connecting = true;
            await this.sendError(err);
            await this.setStatus(Status.Reconnecting);
            setTimeout(async () => {
                let openErr = await this.doOpen();
                if (openErr == OpenError.None) {
                    await this.setStatus(Status.Connected);
                    this._connecting = false;
                } else {
                    if (attempt > maxReconnectAttempts) {
                        this._connecting = false;
                    } else {
                        if (openErr == OpenError.InvalidSessionError || openErr == OpenError.InvalidApiKeyError) {
                            await this.close();
                            return;
                        }
                        await this.reconnect(new Error('Reconnect failed: ' + openErr), ++attempt);
                    }
                }
            }, SessionImpl.getNextDelay(attempt));
        }
    }

    private static getNextDelay(attempt: number) {
        let index: number
        if (attempt >= reconnectTimeouts.length) {
            index = reconnectTimeouts.length - 1
        } else {
            index = attempt
        }
        return reconnectTimeouts[index] * 1000;
    }

    private async setupSendIntervals() {
        this._sendTokenInterval = setInterval(async () => {
            while (this._requests.length > 0 && this._status == Status.Connected) {
                let message = this._requests.shift();
                if (message !== undefined) {
                    let success = await this.doSendTokenRequest(message);
                    if (!success) {
                        return;
                    }
                }
            }
        }, 100);
        this._sendTokenCancelInterval = setInterval(async () => {
            while (this._cancelRequests.length > 0 && this._status == Status.Connected) {
                let message = this._cancelRequests.shift();
                if (message != undefined) {
                    let success = await this.doSendCancelTokenRequest(message);
                    if (!success) {
                        return;
                    }
                }
            }
        }, 100);
    }

    private async doSendTokenRequest(message: CaptchaTokenRequest): Promise<boolean> {
        try {
            message.apiKey = this._apiKey;
            message.createdAt = getCurrentUnixTime();
            this._directChannel.publish(this._amqpConfig.directExchangeName, this._amqpConfig.requestTokenRouteKey,
                Buffer.from(JSON.stringify(message)));
            return true;
        } catch (err) {
            await this.sendError(err);
            return false;
        }
    }

    private async doSendCancelTokenRequest(message: CaptchaTokenCancelRequest): Promise<boolean> {
        try {
            message.apiKey = this._apiKey;
            message.createdAt = getCurrentUnixTime();
            this._fanoutChannel.publish(this._amqpConfig.fanoutExchangeName, this._amqpConfig.requestTokenCancelRouteKey,
                Buffer.from(JSON.stringify(message)));
            return true;
        } catch (err) {
            await this.sendError(err);
            return false;
        }
    }

    private isValidSession() {
        return this._client.hasSession(this);
    }
}

function getCurrentUnixTime() {
    return Math.floor(Date.now() / 1000);
}

function replaceAllDashes(key: string): string {
    return key.split("-").join("");
}

function createKeyWithAccessToken(prefix: string, aToken: string): string {
    return prefix + "." + aToken
}

function createKeyWithAccountId(prefix: string, aId: string): string {
    return prefix + "." + aId
}

function createKeyWithAccountIdAndApiKey(prefix: string, aId: string, apiKey: string): string {
    return prefix + "." + aId + "." + apiKey
}
